#ifdef TEST_2

/*
 * Checks to see if system tasks can be created, and if they run in the expected order.
 * Prints 'F' if the order was unexpected. Prints 'C' when complete.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

 int trace[6];
 int trace_index = 0;

void system_task1(){
	trace[trace_index++] = 3;
	Task_Next();
	trace[trace_index++] = 5;
}

void system_task2(){
	trace[trace_index++] = 4;
	Task_Next();
	trace[trace_index++] = 6;
}

extern int r_main(){    
    usart_init();

    int i;

    trace[trace_index++] = 1;
    Task_Create_System(system_task1,3);
    Task_Create_System(system_task2,3);
    trace[trace_index++] = 2;

    //Having two nexts in a row is neccesary. We want to finish with system_tasks 1 &2 before we check the trace
    Task_Next();
    Task_Next();

    for(i=0;i<6;i++){
    	if(trace[i] != i+1){
    		usart_send('F');
    	}
    }
    usart_send('C');

    return 0;
}

#endif