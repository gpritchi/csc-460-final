#ifdef TEST_11

/*
 * Checks to see if Subscriptions work. A RR task subscribes to a service. A Periodic task publishes.
 * The suscribing System task should wait until the publishing task publishes before resuming.
 * Prints out 'F' if test fails. Prints 'C' when complete.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

int trace[8];
int trace_index = 0;
SERVICE *service;

void check_trace(){
	int key[] = {0,0,2,1,1,1,1,1};
	int i;
	for(i=0;i<8;i++){
		if(trace[i] != key[i]){
			usart_send('F');
		}
		//usart_send((char)((int)'0' + trace[i]));//
	}
	usart_send('C');
}

void bar(){
	int i;
	int16_t val;
	Service_Subscribe(service, &val);
	for(i=0;i<5;i++){
		trace[trace_index++] = 1;
		Task_Next();
	}
	check_trace();
}

void foo(){
	trace[trace_index++] = 2;
	Service_Publish(service, 5);
}

extern int r_main(){    
    usart_init();
    service = Service_Init();

    trace[trace_index++] = 0;
    Task_Create_RR(bar, 1);
    Task_Create_Periodic(foo, 0, 1, 1, 5);
	trace[trace_index++] = 0;

    return 0;
}

#endif