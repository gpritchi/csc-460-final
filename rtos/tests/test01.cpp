#ifdef TEST_1

/*
 * Checks to see if Now() can be called, and calls it repeatidly, each time checking
 * to see id the most recent output is greater than the previous output. Prints an 'F'
 * for failure if the condition is violated. Prints 'C' when complete.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

#define TIMES 100

extern int r_main(){    
    usart_init();
    

	int j;
	uint16_t x = 0;
	uint16_t y = 0;

    for(j = 0; j < TIMES; j++){
        y = Now();

        if(y < x){
        	usart_send('F');
        }
        x = y;
        _delay_ms(25);
    
    }
    usart_send('C');
    return 0;
}

#endif